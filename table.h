/* table.h - MeowMeow, a stream encoder/decoder */


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "purr", "purR", "puRr", "puRR", \
		                   "pUrr", "pUrR", "pURr", "pURR", \
                       "Purr", "PurR", "PuRr", "PuRR", \
		                   "PUrr", "PUrR", "PURr", "PURR" }

#endif	/* _TABLE_H */
